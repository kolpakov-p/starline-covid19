const path = require("path");

module.exports = {
  publicPath: '/starline-covid19/',
  lintOnSave: process.env.NODE_ENV !== 'production',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = 'Антикризисное предложение по StarLine в Самаре';
        args[0].meta = {
          keywords: 'установить старлайн самара, купить старлайн с установкой, установка сигнализации самара, starline самара',
          description: 'Анти-кризисное предложение по установке сигнализации StarLine с запуском в Самаре'
        };
        args[0].template = path.resolve(__dirname, "./src/index.template.html");
        return args
      })
  },
  outputDir: path.resolve(__dirname, "./public"),
}