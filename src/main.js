import Vue from "vue";
import App from "./App.vue";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import Vuelidate from "vuelidate";

import { library } from "@fortawesome/fontawesome-svg-core";
// internal icons
import {
	faCalendarAlt,
	faCaretUp,
	faCaretDown,
	faCaretLeft,
	faStar,
	faPhone,
	faExclamationTriangle,
	faExclamationCircle,
	faInfoCircle,
	faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
	faCalendarAlt,
	faCaretUp,
	faCaretDown,
	faCaretLeft,
	faStar,
	faPhone,
	faExclamationTriangle,
	faExclamationCircle,
	faInfoCircle,
	faCheckCircle
);
Vue.component("vue-fontawesome", FontAwesomeIcon);

Vue.use(Buefy, {
	defaultIconComponent: "vue-fontawesome",
	defaultIconPack: "fas",
});

Vue.use(Vuelidate);

Vue.config.productionTip = false;

import vueSmoothScroll from "vue2-smooth-scroll";
Vue.use(vueSmoothScroll);

new Vue({
	render: (h) => h(App),
}).$mount("#app");
