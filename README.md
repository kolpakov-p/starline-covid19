# Лэндинг

Простой лэндинг с антикризисным предложением установки сигнализации Starline.

Стэк:
- [Vue.js SPA](https://github.com/vuejs/vue)
- [Babel](https://github.com/babel/babel) (для совместимости)
- [Buefy](https://github.com/buefy/buefy) (как набор элементов интерфейса)

## Развертывание боевого проекта
```
npm install
npm run build
```

### Использование в разработке
```
npm install
npm run serve
```